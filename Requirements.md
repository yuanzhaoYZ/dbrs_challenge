# dbrs_challenge



The first thing you should do is thoroughly read this writeup and email us an outline of what you’re planning on doing. Roughly, how you’re going to architect things. After you email us your initial plans, feel free to get started on the project.
 
If you have any questions, please email us. If I don’t promptly answer, please make assumptions or work around the issues you face and move on. Please document what you decided regarding these assumptions/workarounds.
 
Perhaps it goes without saying, readability and understandability is just as important to us as correctness. Try not to include cruft. Data cleaning is important to us. Please consider the fact that we have to get onboarded with your project so adding good documentation for us to follow to set it up is nice to have.
 
There are no trick questions or purposeful puzzles. If something isn’t clear or is ambiguously stated, I just failed to state it clearly.
  
## Part A.
Create a project using Python and tools in the Python ecosystem (preferably pandas) to take some data and answer some questions. Data sourcing, cleaning, and exploration is part of the project so _show your work_.
 
Using the 311 Service Request data from Open Data NYC ( [https://data.cityofnewyork.us/Social-Services/311-Service-Requests-from-2010-to-Present/erm2-nwe9](https://data.cityofnewyork.us/Social-Services/311-Service-Requests-from-2010-to-Present/erm2-nwe9) ) answers following questions for complaints in 2017. Use other datasets as needed, for population data, for data cleaning, etc.
 
* Consider only the 10 most common overall complaint types. For each borough, how many of each of those 10 types were there in 2017
* Consider only the 10 most common overall complaint types.  For the 10 most populous zip codes, how many of each of those 10 types were there in 2017?
* Considering all complaint types. Which boroughs are the biggest “complainers” relative to the size of the population in 2017? Meaning, calculate a complaint-index that adjusts for population of the borough. 

Commit the code to a git repo. In case you can’t get Part B to work, at least get this repo pushed up to Github/Gitlab or the like so you can share it with us.
 
Notes/hints:
* For the 311 data, programmatically source the data. Either:
	* Download the full dataset  https://data.cityofnewyork.us/api/views/erm2-nwe9/rows.csv  and somehow filter down to the 2017 data.
[https://data.cityofnewyork.us/api/views/erm2-nwe9/rows.csv](https://data.cityofnewyork.us/api/views/erm2-nwe9/rows.csv)
	* Use the [Socrata Open Data API](https://dev.socrata.com/foundry/data.cityofnewyork.us/fhrw-4uyv)
* For population by zip code you can use  https://blog.splitwise.com/2013/09/18/the-2010-us-census-population-by-zip-code-totally-free/ . You can just pretend ZCTA == zip and it’s ok that it’s 2010 census data although we’re asking about 2017 complaint data.
* Feel free to use other datasets if you find the need. For example, is there a way to clean up “Unspecified” Boroughs?
* Use Jupyter if you can, consider using[https://hub.docker.com/r/jupyter/datascience-notebook/](https://hub.docker.com/r/jupyter/datascience-notebook/)
 
## Part B
 
Using Docker and Docker Compose (or minikube if you’re familiar with that), setup the above project so that it’s easy for us to run your project. With some minimum amount of directions, we should be able launch the docker container and run the project to download the data and run the code to answer the questions. Example instructions:
 
* `docker-compose up`
* open the Jupyter notebook in a browser· 
* open `data_ingest_clean.ipynb` and run all cells
* open `311_data_explore.ipynb` and run all cells


