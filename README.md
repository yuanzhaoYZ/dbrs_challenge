# DBRS Challenge

Here is my submission to the coding challenge. 
Thanks for the opportunity, I'm looking forward to the next steps in this process.


## Notebook Overview

In [data_ingest_clean.ipynb](./notebooks/data_ingest_clean.ipynb), I did two things:
* Use pyspark to read the 311 csv file and filter it down to 2017 data and save it as a CSV file
* Then I use pandas
    * load the 2017 CSV file
    * clean up the zipcode, fill missing borough names
    * answer all three questions



## How to review my submission

* If you just wanted to review my notebook, please open [data_ingest_clean.html](./notebooks/data_ingest_clean.html) directly in a browser.


* You can also run the live notebook with docker-compose:

    * `docker-compose up --build`
    * open the [data_ingest_clean.ipynb](http://localhost:8889/notebooks/notebooks/data_ingest_clean.ipynb) in a browser and run all cells



If you have any questions, please feel free to email me at <zhao.yuan.work@gmail.com>. Thanks for your time to review my submission.

